#!/bin/bash

# Add the printer
lpadmin -p "LaserJet 2100" -E -m "gutenprint.5.2://hp-lj_2100/expert" -v "usb://HP/LaserJet%202100%20Series"

# Start cups
exec /usr/sbin/cupsd -f
